// npm i fs-extra
// npm i v8-profiler-node8
// npm i @typescript-eslint/typescript-estree
// npm i astring
// npm i eslint

const process = require('process');
const fs = require('fs-extra');
const path = require('path');
const config = require('./eice.config.json');
const shell = require('shelljs');
const esprima = require('@typescript-eslint/typescript-estree');
const traverser = require('eslint/lib/util/traverser');
const { generate } = require('astring');

const PROJECT_DIR = './'
const NEW_SRC_DIR = `${PROJECT_DIR}/eice-injected`;

let exitCode = 0;
(() => {
  for (const inject of config['eice-inject']) {
    fs.removeSync(NEW_SRC_DIR);
    const allFileOrDirs = fs.readdirSync(PROJECT_DIR);
    fs.mkdirSync(NEW_SRC_DIR);
    allFileOrDirs
      .filter(f => f !== 'node_modules')
      .forEach(fileOrDir => {
        fs.copySync(`${PROJECT_DIR}/${fileOrDir}`, `${NEW_SRC_DIR}/${fileOrDir}`);
      });
    fs.symlinkSync(`../node_modules`, `${NEW_SRC_DIR}/node_modules`);

    // inject
    getAllFiles(NEW_SRC_DIR)
      .map(filePath => path.relative(NEW_SRC_DIR, filePath))
      .forEach(filePath => {
        if ((new RegExp(inject.pattern)).test(filePath)) {
          eval(inject.function)(`${NEW_SRC_DIR}/${filePath}`);
        }
      });

    // preprocess
    if (!inject.preprocess) return;
    for (const preprocess of inject.preprocess) {
      const { type, filePattern, fromStr, toStr } = preprocess;
      if (type === 'replace') {
        const allFiles = getAllFiles(NEW_SRC_DIR)
          .map(filePath => path.relative(NEW_SRC_DIR, filePath))
          .filter(filePath => (new RegExp(filePattern)).test(filePath))
          .forEach(filePath => {
            content = fs.readFileSync(`${NEW_SRC_DIR}/${filePath}`, 'utf-8');
            fs.writeFileSync(`${NEW_SRC_DIR}/${filePath}`, content.split(fromStr).join(toStr));
          });
      }
      if (type === 'rename') {
        const allFiles = getAllFiles(NEW_SRC_DIR)
          .map(filePath => path.relative(NEW_SRC_DIR, filePath))
          .filter(filePath => (new RegExp(filePattern)).test(filePath))
          .forEach(filePath => {
            fs.moveSync(`${NEW_SRC_DIR}/${filePath}`, `${NEW_SRC_DIR}/${filePath.split(fromStr).join(toStr)}`)
          });
      }
    }

    // script
    const script = inject.script;
    const shellString = shell.exec(`cd ${NEW_SRC_DIR} && ${script}`);
    if (shellString.code !== 0 && inject.keepExitCode) {
      exitCode = shellString.code;
    }

    // persist
    inject.persist.forEach(filePath => {
      if (!fs.existsSync(`${PROJECT_DIR}/eice`)) {
        fs.mkdirSync(`${PROJECT_DIR}/eice`);
      }
      fs.copySync(`${NEW_SRC_DIR}/${filePath}`, `${PROJECT_DIR}/eice/${filePath}`);
    });
  }
})();
process.exit(exitCode);


function injectProfilingHeader(filePath) {
  const profilingHeader = `
    const _eice_profiler = require('v8-profiler-node8');
    const _eice_fs = require('fs');
    const _eice_process = require('process');
    const _eice_profileId = 'profiling';
    _eice_profiler.startProfiling(_eice_profileId, true);

    _eice_process.on('exit', () => { 
      let profile = _eice_profiler.stopProfiling(_eice_profileId);
      _eice_fs.writeFileSync(_eice_profileId + '.cpuprofile', JSON.stringify(profile));
      console.log('[EICE] Profiler data written');
    });
    `.split('\n').join(' ');
  content = fs.readFileSync(filePath, 'utf-8');
  fs.writeFileSync(filePath, `${profilingHeader}${content}`);
}

function getAllFiles(dir) {
  return fs.readdirSync(dir).filter(f => !['node_modules', '.git'].includes(f)).reduce((files, file) => {
    const name = path.join(dir, file);
    const isDirectory = fs.statSync(name).isDirectory();
    return isDirectory ? [...files, ...getAllFiles(name)] : [...files, name];
  }, []);
}

function injectValueLikeHeader(filePath) {
  console.log(`processing ${filePath}`);

  const reservedWords = ['break', 'case', 'catch', 'continue', 'debugger', 'default', 'delete', 'do', 'else', 'finally', 'for', 'function', 'if', 'in', 'instanceof', 'new', 'return', 'switch', 'this', 'throw', 'try', 'typeof', 'var', 'void', 'while', 'with'];

  const content = fs.readFileSync(filePath, 'utf-8');
  const ast = esprima.parse(content, {
    loc: true,
  });

  let linesAfter = {};
  let linesBefore = {};
  traverser.traverse(ast, {
    enter: function (node, parent) {
      if (
        [
          'LabeledStatement',
          'ExpressionStatement',
          'AssignmentExpression',
          'Statement',
          'VariableDeclaration',
          'ReturnStatement'].includes(node.type)
        && ['BlockStatement'].indexOf(node.type) === -1) {

        // get variable name
        // console.log(JSON.stringify(e, null, 4));
        // console.log(e);
        let variables = [];
        traverser.traverse(node, {
          enter: function (_node, _parent) {
            switch (_node.type) {
            case 'MemberExpression':
              // skip functions
              // if (_parent.type === 'CallExpression') break;
              variables.push(_node.object.name);
              variables.push(generate(_node));
              // variables.push(memberExpressionToString(_node));
              // const code = generate(_node);
              // if (!code.includes('('))
                // variables.push(code);
              break;
            case 'ThisExpression':
              variables.push('this');
              break;
            case 'CallExpression':
              variables = variables.concat(
                _node.arguments.map(a => a.name)
              );
              break;
            case 'ObjectExpression':
              variables = variables.concat(_node.properties.map(p => p.value.name));
              break;
            case 'Identifier':
              variables.push(_node.name);
              break;
            }
          }
        });
        variables = [...new Set(
          variables
            .filter(v => v)
            .filter(v => !reservedWords.includes(v))
        )];
        linesAfter[node.loc.end.line] = {
          variables,
          type: node.type
        };
        linesBefore[node.loc.start.line] = {
          variables,
          type: node.type
        };
      }
    }
  });
  
  const contentLines = content.split('\n');
  const n = contentLines.length;

  let modified = [];
  modified.push(`
const _eice_util = require('util');
Object.stringify = function(value, space) {
  var cache = [];
  var output = JSON.stringify(value, function (key, value) {
    if (typeof value === 'function') {
      return value.toString();
    }
    if (typeof value === 'object' && value !== null) {
      if (cache.indexOf(value) !== -1) {
        // Circular reference found, discard key
        return;
      }
      // Store value in our collection
      cache.push(value);
    }
    return value;
  }, space)
  cache = null; // Enable garbage collection
  return output;
}
if (typeof global.DAINJECT == 'undefined') {
  // init DAINJECT
  global.DAINJECT = {
      before: {},
      after: {},
      injectBefore: function(DAINJECTCURRENT, line, data) {
        if (!DAINJECTCURRENT.before.hasOwnProperty(line) || DAINJECTCURRENT.before[line] === undefined) {
          DAINJECTCURRENT.before[line] = data;
        }
      },
      injectAfter: function(DAINJECTCURRENT, line, data) {
        if (!DAINJECTCURRENT.after.hasOwnProperty(line) || DAINJECTCURRENT.before[line] === undefined) {
          DAINJECTCURRENT.after[line] = data;
        }
      },
      done: function() {
        require('fs').writeFileSync('DA.before.json', Object.stringify(global.DAINJECT.before, 2))
        require('fs').writeFileSync('DA.after.json', Object.stringify(global.DAINJECT.after, 2))
      }
  }
}
const DAINJECT = global.DAINJECT;
const DAINJECTCURRENT = {
  before: DAINJECT.before[require('path').relative(require('process').cwd(), __filename)] = {},
  after: DAINJECT.after[require('path').relative(require('process').cwd(), __filename)] = {}
};
require('process').on('exit', (code) => {
  DAINJECT.done();
});

  `);
  for (let i = 0; i < n; ++i) {
    if (linesBefore[i + 1]) {
      modified.push(`\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tDAINJECT.injectBefore(DAINJECTCURRENT, ${i + 1}, {${
        linesBefore[i + 1]
          .variables
          .map(v => `'${v.replace(/'/g, '\\\'')}': (() => { try { return _eice_util.inspect(${v}); } catch (e) { return undefined; }})()`)
      }});// line ${i + 1}, type: ${linesBefore[i + 1].type}`);
    }
    modified.push(contentLines[i]);
    if (linesAfter[i + 1]) {
      modified.push(`\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tDAINJECT.injectAfter(DAINJECTCURRENT, ${i + 1}, {${
        linesAfter[i + 1]
          .variables
          .map(v => `'${v.replace(/'/g, '\\\'')}': (() => { try { return _eice_util.inspect(${v}); } catch (e) { return undefined; }})()`)
        }});// line ${i + 1}, type: ${linesAfter[i + 1].type}`);
    }
  }
  fs.writeFileSync(filePath, modified.join('\n'));
}